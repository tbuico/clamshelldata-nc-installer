#!/bin/bash

# Tech and Me © - 2017, https://www.techandme.se/
# Tweaks to Original scripts by Clamshell Data, https://clamshelldata.com

printf "%s\n""\e[0;32m" && echo  "Starting Nextcloud Login, please be patient." && printf "\e[0m\n"

# shellcheck disable=2034,2059
true
# shellcheck source=lib.sh
FIRST_IFACE=1 && CHECK_CURRENT_REPO=1 . <(curl -sL https://raw.githubusercontent.com/nextcloud/vm/master/lib.sh)
unset FIRST_IFACE
unset CHECK_CURRENT_REPO

# Custom Variables
GITLAB="https://gitlab.com/hungrykanamit/Nextcloud/raw/master/Nextcloud-files/Scripts/"

# Custom Functions
  # run_gitlab_script name_of_script
  run_gitlab_script() {
      # Get ${1} gitlab
      rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
      if wget -q "${GITLAB}/${1}.sh" -P "$SCRIPTS"
      then
          bash "${SCRIPTS}/${1}.sh"
          rm -f "${SCRIPTS}/${1}.sh"
      elif wget -q "${GITLAB}/${1}.php" -P "$SCRIPTS"
      then
          php "${SCRIPTS}/${1}.php"
          rm -f "${SCRIPTS}/${1}.php"
      elif wget -q "${GITLAB}/${1}.py" -P "$SCRIPTS"
      then
          python "${SCRIPTS}/${1}.py"
          rm -f "${SCRIPTS}/${1}.py"
      else
          echo "Downloading ${1} failed"
          echo "Script failed to download. Please run: 'sudo wget ${GITLAB}/${1}.sh|php|py' again."
          sleep 3
      fi
  }

  # download_app_script name_of_script
  download_app_script() {
      # Get ${1} script
      rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
      if ! { wget -q "${APP}/${1}.sh" -P "$SCRIPTS" || wget -q "${APP}/${1}.php" -P "$SCRIPTS" || wget -q "${APP}/${1}.py" -P "$SCRIPTS"; }
      then
          echo "{$1} failed to download. Please run: 'sudo wget ${APP}/${1}.sh|.php|.py' again."
          echo "If you get this error when running the nextcloud-startup-script then just re-run it with:"
          echo "'sudo bash $SCRIPTS/nextcloud-startup-script.sh' and all the scripts will be downloaded again"
          exit 1
      fi
  }

  # download_gitlab_script name_of_script
  download_gitlab_script() {
      # Get ${1} script
      rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
      if ! { wget -q "${GITLAB}/${1}.sh" -P "$SCRIPTS" || wget -q "${GITLAB}/${1}.php" -P "$SCRIPTS" || wget -q "${GITLAB}/${1}.py" -P "$SCRIPTS"; }
      then
          echo "{$1} failed to download. Please run: 'sudo wget ${GITLAB}/${1}.sh|.php|.py' again."
          echo "If you get this error when running the nextcloud-startup-script then just re-run it with:"
          echo "'sudo bash $SCRIPTS/nextcloud-startup-script.sh' and all the scripts will be downloaded again"
          exit 1
      fi
  }

printf "%s\n""\e[0;32m" && echo "Updateing scripts from the web..." && printf "\e[0m\n"
# Most of the shell scripts in static (.sh)
download_static_script security
download_static_script update
download_static_script trusted
download_static_script setup_secure_permissions_nextcloud
download_static_script change_mysql_pass
download_static_script nextcloud
download_static_script update-config
download_static_script index
download_static_script history

# Most of the shell scripts in app (.sh)
download_app_script collabora
download_app_script nextant
download_app_script passman
download_app_script spreedme

# Most of the shell scripts in gitlab (.sh)
download_gitlab_script instruction
download_gitlab_script timezone
download_gitlab_script change-NCADMIN-password
download_gitlab_script fail2ban
download_gitlab_script sync-all-scripts
download_gitlab_script nextcloud-first-run-startup
download_gitlab_script nextcloud-every-startup

# Get temporary-fix.sh
if [ -f ${SCRIPTS}/temporary-fix.sh ]
then
    rm ${SCRIPTS}/temporary-fix.sh
    wget -q "${STATIC}/temporary-fix.sh" -P ${SCRIPTS}
else
    wget -q "${STATIC}/temporary-fix.sh" -P ${SCRIPTS}
fi

# Lets Encrypt
if [ -f "$SCRIPTS"/activate-ssl.sh ]
then
    rm "$SCRIPTS"/activate-ssl.sh
    wget -q $LETS_ENC/activate-ssl.sh -P "$SCRIPTS"
else
    wget -q $LETS_ENC/activate-ssl.sh -P "$SCRIPTS"
fi
if [ ! -f "$SCRIPTS"/activate-ssl.sh ]
then
    echo "activate-ssl failed"
    echo "Script failed to download. Please run: 'sudo bash $SCRIPTS/nextcloud-startup-script.sh' again."
    exit 1
fi

mv $SCRIPTS/index.php $HTML/index.php && rm -f $HTML/html/index.html
chmod 750 $HTML/index.php && chown www-data:www-data $HTML/index.php

printf "%s\n""\e[0;32m" && echo "Securing permissions" && printf "\e[0m\n"
# Change 000-default to $WEB_ROOT
sed -i "s|DocumentRoot /var/www/html|DocumentRoot $HTML|g" /etc/apache2/sites-available/000-default.conf

# Make $SCRIPTS excutable
chmod +x -R $SCRIPTS
chown root:root -R $SCRIPTS

# Allow $UNIXUSER to run figlet script
chown "$UNIXUSER":"$UNIXUSER" "$SCRIPTS/nextcloud.sh"

# Add temporary fix if needed
bash $SCRIPTS/temporary-fix.sh
rm "$SCRIPTS"/temporary-fix.sh

printf "%s\n""\e[0;32m" && echo "Updateing Ubuntu" && printf "\e[0m\n"
# Upgrade Ubuntu server
apt -y update
apt -y full-upgrade

# Prepare every bootup
#check_command run_static_script change-ncadmin-profile
check_command run_gitlab_script change-root-profile_every-run

printf "%s\n""\e[0;32m" && echo "Cleaning up" && printf "\e[0m\n"
# Cleanup 1
sudo -u www-data php "$NCPATH/occ" maintenance:repair
rm -f "$SCRIPTS/ip.sh"
rm -f "$SCRIPTS/test_connection.sh"
#rm -f "$SCRIPTS/instruction.sh"
rm -f "$NCDATA/nextcloud.log"
#rm -f "$SCRIPTS/nextcloud-startup-script.sh"
find /root "/home/$UNIXUSER" -type f \( -name '*.sh*' -o -name '*.html*' -o -name '*.tar*' -o -name '*.zip*' \) -delete
sed -i "s|instruction.sh|nextcloud.sh|g" "/home/$UNIXUSER/.bash_profile"

truncate -s 0 \
    /root/.bash_history \
    "/home/$UNIXUSER/.bash_history" \
    /var/spool/mail/root \
    "/var/spool/mail/$UNIXUSER" \
    /var/log/apache2/access.log \
    /var/log/apache2/error.log \
    /var/log/cronjobs_success.log

sed -i "s|sudo -i||g" "/home/$UNIXUSER/.bash_profile"
cat << RCLOCAL > "/etc/rc.local"
#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

exit 0

RCLOCAL
clear

#printf "%s\n""\e[0;32m" && echo "Updateing Nextcloud" && printf "\e[0m\n"
# Upgrade Nextcloud
#bash $SCRIPTS/update.sh

# Cleanup 2
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb
CLEARBOOT=$(dpkg -l linux-* | awk '/^ii/{ print $2}' | grep -v -e "$(uname -r | cut -f1,2 -d"-")" | grep -e "[0-9]" | xargs sudo apt -y purge)
echo "$CLEARBOOT"
# Delete MySQL password from /root/.my.cnf (for security resons)
sed -i '/password/d' /root/.my.cnf

ADDRESS2=$(grep "address" /etc/network/interfaces | awk '$1 == "address" { print $2 }')
# Success!

printf "%s\n""\e[0;32m" && echo "Running instruction.sh" && printf "\e[0m\n"

# Run instruction.sh
bash $SCRIPTS/instruction.sh
