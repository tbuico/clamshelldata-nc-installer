#!/bin/bash

# Tech and Me © - 2017, https://www.techandme.se/
# Tweaks to Original scripts by Clamshell Data, https://clamshelldata.com

ADDRESS3=$(hostname -I | cut -d ' ' -f 1)
printf "%s\n""\e[0;32m"
echo    "+-----------------------------------------------------------------------+"
printf "|    \e[0mThanks for using this Nextcloud server!\e[0;32m                            |\n"
echo    "|                                                                       |"
printf "|    \e[0mLogin to Nextcloud with http: \e[0;36m\http://$ADDRESS3/login\\e[0;32m       |\n"
printf "|    \e[0mLogin to Nextcloud with https: \e[0;36m\https://$ADDRESS3/login\\e[0;32m     |\n"
echo    "|                                                                       |"
printf "|    \e[0mIt is recommended to change the default user.\e[0;32m                      |\n"
printf "|    \e[0mDo this by adding another admin user, log out from ncadmin,\e[0;32m        |\n"
printf "|    \e[0mand login with your new user, then delete ncadmin.\e[0;32m                 |\n"
echo    "|                                                                       |"
printf "|    \e[0mTo run the first boot script again type:\e[0;32m                           |\n"
printf "|    \e[0;36m     'sudo bash /var/scripts/nextcloud-first-run-startup.sh'\e[0;32m       |\n"
#echo    "|                                                                       |"
#printf "|    \e[0mEvery root login, the update script will run automatically\e[0;32m         |\n"
echo    "|                                                                       |"
printf "|    \e[0mTo update Nextcloud manually, type:\e[0;32m                                |\n"
printf "|    \e[0;36m     'sudo bash /var/scripts/update.sh'\e[0;32m                            |\n"
echo    "|                                                                       |"
printf "|    \e[0mTo login to MySQL just type: \e[0;36m'mysql -u root -p'\e[0;32m                    |\n"
printf "|    \e[0mand enter the password for mysql which you were asked to record.\e[0;32m   |\n"
echo    "|                                                                       |"
echo    "+-----------------------------------------------------------------------+"
printf "\e[0m\n"

exit 0
