#!/bin/bash

# Requirements:
#Install-Nextcloud-on-Ubuntu-16.04.sh executed successfully

# Set Variables
echo "Setting Variables"
WWW_ROOT=/var/www
NCPATH=$WWW_ROOT/nextcloud
NCDATA=/var/ncdata
SCRIPTS=/var/scripts
IFACE=$(lshw -c network | grep "logical name" | awk '{print $3; exit}')
CLEARBOOT=$(dpkg -l linux-* | awk '/^ii/{ print $2}' | grep -v -e `uname -r | cut -f1,2 -d"-"` | grep -e [0-9] | xargs sudo apt -y purge)
PHPMYADMIN_CONF="/etc/apache2/conf-available/phpmyadmin.conf"
GITHUB_REPO="https://raw.githubusercontent.com/nextcloud/vm/master"
STATIC="https://raw.githubusercontent.com/nextcloud/vm/master/static"
LETS_ENC="https://raw.githubusercontent.com/nextcloud/vm/master/lets-encrypt"
UNIXUSER=$SUDO_USER
GITLAB="https://gitlab.com/hungrykanamit/Nextcloud/raw/master/Nextcloud/Scripts/"
SSL_CONF="/etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf"
HTTP_CONF="/etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf"
NCREPO="https://download.nextcloud.com/server/releases/"
NCVERSION=$(curl -s $NCREPO | tac | grep unknown.gif | sed 's/.*"nextcloud-\([^"]*\).zip.sha512".*/\1/;q')
STABLEVERSION="nextcloud-$NCVERSION"
GPGDIR=/tmp/gpg
OpenPGP_fingerprint='28806A878AE423A28372792ED75899B9A724937A'

# Set Functions
echo "Setting Functions"
#ask_yes_or_no
function ask_yes_or_no() {
    read -p "$1 ([y]es or [N]o): "
    case $(echo $REPLY | tr '[A-Z]' '[a-z]') in
        y|yes) echo "yes" ;;
        *)     echo "no" ;;
    esac
}

# Check where the best mirrors are and update
#printf "\nTo make downloads as fast as possible when updating you should have mirrors that are as close to you as possible.\n"
#echo "This server comes with mirrors based on servers in that where used when the template was released and packaged."
#echo "We give you the option to change the mirrors based on where this is currently installed."
#echo "Checking current mirror..."
#printf "Your current server repository is:  ${Cyan}$REPO${Color_Off}\n"
#if [[ "no" == $(ask_yes_or_no "Do you want to try to find a better mirror?") ]]
#then
#    echo "Keeping $REPO as mirror..."
#    sleep 1
#else
#    echo "Locating the best mirrors..."
#    apt update -q4 #& spinner_loading
#    apt install python-pip -y
#    pip install \
#        --upgrade pip \
#        apt-select
#    apt-select -m up-to-date -t 5 -c
#    sudo cp /etc/apt/sources.list /etc/apt/sources.list.backup && \
#    if [ -f sources.list ]
#    then
#        sudo mv sources.list /etc/apt/
#    fi
#fi
#
#echo

echo "Updateing scripts from the web to be able to run the first setup..."

# Update temporary-fix.sh
if [ -f ${SCRIPTS}/temporary-fix.sh ]
then
    rm ${SCRIPTS}/temporary-fix.sh
    wget -q "${STATIC}/temporary-fix.sh" -P ${SCRIPTS}
else
    wget -q "${STATIC}/temporary-fix.sh" -P ${SCRIPTS}
fi

# Update security.sh
if [ -f ${SCRIPTS}/security.sh ]
then
    rm ${SCRIPTS}/security.sh
    wget -q "${STATIC}/security.sh" -P ${SCRIPTS}
else
    wget -q "${STATIC}/security.sh" -P ${SCRIPTS}
fi

# Update update.sh
if [ -f ${SCRIPTS}/update.sh ]
then
    rm ${SCRIPTS}/update.sh
    wget -q "${STATIC}/update.sh" -P ${SCRIPTS}
else
    wget -q "${STATIC}/update.sh" -P ${SCRIPTS}
fi

# Update trusted.sh
if [ -f ${SCRIPTS}/trusted.sh ]
then
    rm ${SCRIPTS}/trusted.sh
    wget -q ${STATIC}/trusted.sh -P ${SCRIPTS}
else
    wget -q ${STATIC}/trusted.sh -P ${SCRIPTS}
fi

# Update nextcloud.sh
if [ -f ${SCRIPTS}/nextcloud.sh ]
then
    rm ${SCRIPTS}/nextcloud.sh
    wget -q "${STATIC}/nextcloud.sh" -P ${SCRIPTS}
else
    wget -q "${STATIC}/nextcloud.sh" -P ${SCRIPTS}
fi

# Update timezone.sh
if [ -f ${SCRIPTS}/timezone.sh ]
then
    rm ${SCRIPTS}/timezone.sh
    wget -q "${GITLAB}/timezone.sh" -P ${SCRIPTS}
else
    wget -q "${GITLAB}/timezone.sh" -P ${SCRIPTS}
fi

# Update update-config.php
if [ -f ${SCRIPTS}/update-config.php ]
then
    rm ${SCRIPTS}/update-config.php
    wget -q "${STATIC}/update-config.php" -P ${SCRIPTS}
else
    wget -q "${STATIC}/update-config.php" -P ${SCRIPTS}
fi

# Update index.php
if [ -f ${SCRIPTS}/index.php ]
then
    rm ${SCRIPTS}/index.php
    wget -q "${STATIC}/index.php" -P ${SCRIPTS}
else
    wget -q "${STATIC}/index.php" -P ${SCRIPTS}
fi

# Lets Encrypt
if [ -f "$SCRIPTS"/activate-ssl.sh ]
then
    rm "$SCRIPTS"/activate-ssl.sh
    wget -q $LETS_ENC/activate-ssl.sh -P "$SCRIPTS"
else
    wget -q $LETS_ENC/activate-ssl.sh -P "$SCRIPTS"
fi
if [ ! -f "$SCRIPTS"/activate-ssl.sh ]
then
    echo "Script 'activate-ssl.sh' failed."
    echo "Script failed to download. Please run: 'sudo bash $SCRIPTS/nextcloud-startup-script.sh' again."
    exit 1
fi

mv $SCRIPTS/index.php $HTML/index.php && rm -f $HTML/html/index.html
chmod 750 $HTML/index.php && chown www-data:www-data $HTML/index.php

# Sets secure permissions after upgrade
if [ -f $SCRIPTS/Nextcloud_Set-File-Permissions.sh ]
then
    rm $SCRIPTS/Nextcloud_Set-File-Permissions.sh
    wget -q $GITLAB/Nextcloud_Set-File-Permissions.sh -P $SCRIPTS
else
    wget -q $GITLAB/Nextcloud_Set-File-Permissions.sh -P $SCRIPTS
fi
if [ -f $SCRIPTS/Nextcloud_Set-File-Permissions.sh ]
then
    sleep 0.1
else
    echo "setup_secure_permissions_nextcloud failed"
    echo "Script failed to download. Please run: 'sudo bash /var/scripts/nextcloud-first-run-startup.sh' again."
    exit 1
fi

# Change 000-default to $WEB_ROOT
sed -i "s|DocumentRoot /var/www/html|DocumentRoot $HTML|g" /etc/apache2/sites-available/000-default.conf

# Make $SCRIPTS excutable
chmod +x -R $SCRIPTS
chown root:root -R $SCRIPTS

# Allow $UNIXUSER to run figlet script
chown "$UNIXUSER":"$UNIXUSER" "$SCRIPTS/nextcloud.sh"

clear
echo "+--------------------------------------------------------------------+"
echo "| This script will configure your Nextcloud and activate SSL.        |"
echo "| It will also do the following:                                     |"
echo "|                                                                    |"
echo "| - Set new keyboard layout                                          |"
echo "| - Change timezone        		                                   |"
echo "| - Generate new SSH keys for the server                             |"
echo "| - Generate new MySQL password                                      |"
echo "| - Configure UTF8mb4 (4-byte support for MySQL)                     |"
echo "| - Install phpMyadmin and make it secure                            |"
echo "| - Install selected apps and automatically configure them           |"
echo "| - Detect and set hostname                                          |"
echo "| - Upgrade your system and Nextcloud to latest version              |"
echo "| - Set secure permissions to Nextcloud                              |"
#echo "| - Set new passwords to Linux and Nextcloud                         |"                                                 |"
#echo "| - Set static IP to the system (you have to set the same IP in      |"
#echo "|   your router) https://www.techandme.se/open-port-80-443/          |"
#echo "|   We don't set static IP if you run this on a *remote* VPS.        |"
echo "|                                                                    |"
echo "|   The script will take about 10 minutes to finish,                 |"
echo "|   depending on your internet connection.                           |"
echo "|                                                                    |"
echo "+--------------------------------------------------------------------+"
echo "Press enter to continue"
IFS= read -r REPLY
clear

# Set keyboard layout
echo "Current keyboard layout is $(localectl status | grep "Layout" | awk '{print $3}')"
if [[ "no" == $(ask_yes_or_no "Do you want to change keyboard layout?") ]]
then
    echo "Not changing keyboard layout..."
    sleep 1
    clear
else
    dpkg-reconfigure keyboard-configuration
clear
fi

# Change Timezone
if [[ "yes" == $(ask_yes_or_no "Do you want to change your timezone?") ]]
then
	dpkg-reconfigure tzdata
	sleep 3
	echo "Press enter to continue"
	IFS= read -r REPLY
else
    echo
    echo "OK, Current timezone is $(cat /etc/timezone)"
    echo "However, if you want to run it later, just type: sudo bash $SCRIPTS/timezone.sh"
	echo "Press enter to continue"
	IFS= read -r REPLY
fi
clear

# Pretty URLs
echo "Setting RewriteBase to \"/\" in config.php..."
chown -R www-data:www-data $NCPATH
sudo -u www-data php $NCPATH/occ config:system:set htaccess.RewriteBase --value="/"
sudo -u www-data php $NCPATH/occ maintenance:update:htaccess
bash $SECURE & spinner_loading

# Generate new SSH Keys
printf "\nGenerating new SSH keys for the server...\n"
rm -v /etc/ssh/ssh_host_*
dpkg-reconfigure openssh-server

#mysql_secure_installation
apt -y update
apt -y install expect
read -p "Type the password you want root to use for MySQL here: " NCPASS
echo "MySQL is being secured, this may take a moment..."
SECURE_MYSQL=$(expect -c "
set timeout 10
spawn mysql_secure_installation
expect \"Enter current password for root:\"
send \"nextcloud\r\"
#expect \"Would you like to setup VALIDATE PASSWORD plugin?\"
#send \"n\r\"
expect \"Change the password for root ?\"
send \"y\r\"
send \"$NCPASS\r\"
send \"$NCPASS\r\"
expect \"Remove anonymous users?\"
send \"y\r\"
expect \"Disallow root login remotely?\"
send \"y\r\"
expect \"Remove test database and access to it?\"
send \"y\r\"
expect \"Reload privilege tables now?\"
send \"y\r\"
expect eof
")
echo "$SECURE_MYSQL"
apt -y purge expect

# Set MySQL Variables
NCUSER=oc_ncadmin
NCDB=nextcloud_db

# MySQL oc_ncadmin passwd change
mysql -u root mysql -p$NCPASS -e "update mysql.user set password=PASSWORD('$NCPASS') where User='oc_ncadmin';"
mysql -u root -p$NCPASS -e "flush privileges;"
#echo 'MySQL Password updated successfully for oc_ncadmin user to match root user password.'

# Enable UTF8mb4 (4-byte support)
mysql -u root -p$NCPASS -e "ALTER DATABASE nextcloud_db CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"
sudo -u www-data $NCPATH/occ config:system:set mysql.utf8mb4 --type boolean --value="true"
sudo -u www-data $NCPATH/occ maintenance:repair

echo "MySQL has been secured and the passwords for root & oc-ncadmin have been set to $NCPASS"
echo "Please make a record of this"
echo "Press enter to continue"
IFS= read -r REPLY

###################---------------------------------------------#####################

# Install phpMyadmin
if [ -f ${SCRIPTS}/phpmyadmin_install_ubuntu16.sh ]
then
    rm ${SCRIPTS}/index.php
    wget -q "${STATIC}/phpmyadmin_install_ubuntu16.sh" -P ${SCRIPTS}
else
    wget -q "${STATIC}/phpmyadmin_install_ubuntu16.sh" -P ${SCRIPTS}
fi
if [ ! -f "$SCRIPTS"/phpmyadmin_install_ubuntu16.sh ]
then
    echo "Script 'phpmyadmin_install_ubuntu16.sh' failed."
    echo "Script failed to download. Please run: 'sudo bash $SCRIPTS/nextcloud-startup-script.sh' again."
    exit 1
fi
clear

cat << LETSENC
+-----------------------------------------------+
|  The following script will install a trusted  |
|  SSL certificate through Let's Encrypt.       |
+-----------------------------------------------+
LETSENC

# Let's Encrypt
if [[ "yes" == $(ask_yes_or_no "Do you want to install SSL?") ]]
then
    bash $SCRIPTS/activate-ssl.sh
else
    echo
    echo "OK, but if you want to run it later, just type: sudo bash $SCRIPTS/activate-ssl.sh"
	echo "Press enter to continue"
	IFS= read -r REPLY
fi
clear

whiptail --title "Which apps do you want to install?" --checklist --separate-output "Automatically configure and install selected apps\nSelect by pressing the spacebar" "$WT_HEIGHT" "$WT_WIDTH" 4 \
"Collabora" "(Online editing)   " OFF \
"Nextant" "(Full text search)   " OFF \
"Passman" "(Password storage)   " OFF \
"Spreed.ME" "(Video calls)   " OFF 2>results

while read -r -u 9 choice
do
    case $choice in
        Collabora)
            run_app_script collabora
        ;;

        Nextant)
            run_app_script nextant
        ;;

        Passman)
            run_app_script passman
        ;;

        Spreed.ME)
            run_app_script spreedme
        ;;

        *)
        ;;
    esac
done 9< results
rm -f results
clear

# Add extra security
if [[ "yes" == $(ask_yes_or_no "Do you want to add extra security, based on this: http://goo.gl/gEJHi7 ?") ]]
then
	apt -y update
    bash $SCRIPTS/security.sh
    rm "$SCRIPTS"/security.sh
else
    echo
    echo "OK, but if you want to run it later, just type: sudo bash $SCRIPTS/security.sh"
	echo "Press enter to continue"
	IFS= read -r REPLY
fi
clear

# Fixes https://github.com/nextcloud/vm/issues/58
a2dismod status
service apache2 reload

# Increase max filesize (expects that changes are made in /etc/php/7.0/apache2/php.ini)
VALUE="# php_value upload_max_filesize 513M"
if ! grep -Fxq "$VALUE" $NCPATH/.htaccess
then
        sed -i 's/  php_value upload_max_filesize 513M/# php_value upload_max_filesize 513M/g' $NCPATH/.htaccess
        sed -i 's/  php_value post_max_size 513M/# php_value post_max_size 513M/g' $NCPATH/.htaccess
        sed -i 's/  php_value memory_limit 512M/# php_value memory_limit 512M/g' $NCPATH/.htaccess
fi

# Add temporary fix if needed
bash $SCRIPTS/temporary-fix.sh
rm "$SCRIPTS"/temporary-fix.sh

# Cleanup 1
sudo -u www-data php "$NCPATH/occ" maintenance:repair
#rm -f "$SCRIPTS/ip.sh"
#rm -f "$SCRIPTS/test_connection.sh"
#rm -f "$SCRIPTS/instruction.sh"
rm -f "$NCDATA/nextcloud.log"
#rm -f "$SCRIPTS/nextcloud-startup-script.sh"
find /root "/home/$UNIXUSER" -type f \( -name '*.sh*' -o -name '*.html*' -o -name '*.tar*' -o -name '*.zip*' \) -delete
sed -i "s|instruction.sh|nextcloud.sh|g" "/home/$UNIXUSER/.bash_profile"

truncate -s 0 \
    /root/.bash_history \
    "/home/$UNIXUSER/.bash_history" \
    /var/spool/mail/root \
    "/var/spool/mail/$UNIXUSER" \
    /var/log/apache2/access.log \
    /var/log/apache2/error.log \
    /var/log/cronjobs_success.log

sed -i "s|sudo -i||g" "/home/$UNIXUSER/.bash_profile"
cat << RCLOCAL > "/etc/rc.local"
#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

exit 0

RCLOCAL
clear

# Upgrade system
echo "System will now upgrade..."
bash $SCRIPTS/update.sh

# Cleanup 2
apt autoremove -y
apt autoclean
rm -rf /var/cache/apt/archives/*.deb
CLEARBOOT=$(dpkg -l linux-* | awk '/^ii/{ print $2}' | grep -v -e "$(uname -r | cut -f1,2 -d"-")" | grep -e "[0-9]" | xargs sudo apt -y purge)
echo "$CLEARBOOT"

ADDRESS2=$(grep "address" /etc/network/interfaces | awk '$1 == "address" { print $2 }')
# Success!
clear
printf "%s\n""${Green}"
echo    "+--------------------------------------------------------------------+"
echo    "|      Congratulations! You have successfully installed Nextcloud!   |"
echo    "|                                                                    |"
printf "|         ${Color_Off}Login to Nextcloud in your browser: ${Cyan}\"$ADDRESS2\"${Green}         |\n"
echo    "|                                                                    |"
printf "|         ${Color_Off}Publish your server online! ${Cyan}https://goo.gl/iUGE2U${Green}          |\n"
echo    "|                                                                    |"
printf "|         ${Color_Off}To login to MySQL just type: ${Cyan}'mysql -u root'${Green}               |\n"
echo    "|                                                                    |"
printf "|   ${Color_Off}To update this server just type: ${Cyan}'sudo bash /var/scripts/update.sh'${Green}  |\n"
echo    "|                                                                    |"
echo    "+--------------------------------------------------------------------+"
printf "${Color_Off}\n"

# Set trusted domain in config.php
bash "$SCRIPTS"/trusted.sh
rm -f "$SCRIPTS"/trusted.sh

# Prefer IPv6
sed -i "s|precedence ::ffff:0:0/96  100|#precedence ::ffff:0:0/96  100|g" /etc/gai.conf

# Reboot
#rm -f "$SCRIPTS/nextcloud-startup-script.sh"
echo "Installation finished, press enter to reboot system..."
IFS= read -r REPLY
reboot
