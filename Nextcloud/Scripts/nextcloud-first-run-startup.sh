#!/bin/bash

# Tech and Me © - 2017, https://www.techandme.se/
# Tweaks to Original scripts by Clamshell Data, https://clamshelldata.com

# shellcheck disable=2034,2059
true
# shellcheck source=lib.sh
FIRST_IFACE=1 && CHECK_CURRENT_REPO=1 . <(curl -sL https://raw.githubusercontent.com/nextcloud/vm/master/lib.sh)
unset FIRST_IFACE
unset CHECK_CURRENT_REPO

## Check where the best mirrors are and update (Skipped, not essential and buggy)
#printf "\nTo make downloads as fast as possible when updating you should have mirrors that are as close to you as possible.\n"
#echo "This VM comes with mirrors based on servers in that where used when the VM was released and packaged."
#echo "We recomend you to change the mirrors based on where this is currently installed."
#echo "Checking current mirror..."
#printf "Your current server repository is:  ${Cyan}$REPO${Color_Off}\n"
#
#if [[ "no" == $(ask_yes_or_no "Do you want to try to find a better mirror?") ]]
#then
#    echo "Keeping $REPO as mirror..."
#    sleep 1
#else
#    echo "Locating the best mirrors..."
#    apt update -q4 & spinner_loading
#    apt install python-pip -y
#    pip install \
#        --upgrade pip \
#        apt-select
#    apt-select -m up-to-date -t 5 -c
#    sudo cp /etc/apt/sources.list /etc/apt/sources.list.backup && \
#    if [ -f sources.list ]
#    then
#        sudo mv sources.list /etc/apt/
#    fi
#fi

# Custom Variables
GITLAB="https://gitlab.com/hungrykanamit/Nextcloud/raw/master/Nextcloud/Scripts/"

# Custom Functions
  # run_gitlab_script name_of_script
  run_gitlab_script() {
      # Get ${1} gitlab
      rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
      if wget -q "${GITLAB}/${1}.sh" -P "$SCRIPTS"
      then
          bash "${SCRIPTS}/${1}.sh"
          rm -f "${SCRIPTS}/${1}.sh"
      elif wget -q "${GITLAB}/${1}.php" -P "$SCRIPTS"
      then
          php "${SCRIPTS}/${1}.php"
          rm -f "${SCRIPTS}/${1}.php"
      elif wget -q "${GITLAB}/${1}.py" -P "$SCRIPTS"
      then
          python "${SCRIPTS}/${1}.py"
          rm -f "${SCRIPTS}/${1}.py"
      else
          echo "Downloading ${1} failed"
          echo "Script failed to download. Please run: 'sudo wget ${GITLAB}/${1}.sh|php|py' again."
          sleep 3
      fi
  }

echo
echo  "Updateing scripts from the web to be able to run the first setup..."
# All the shell scripts in static (.sh)
#download_static_script temporary-fix
download_static_script security
download_static_script update
download_static_script trusted
#download_static_script ip
#download_static_script test_connection
download_static_script setup_secure_permissions_nextcloud
download_static_script change_mysql_pass
download_static_script nextcloud
download_static_script update-config
download_static_script index

# Get temporary-fix.sh
if [ -f ${SCRIPTS}/temporary-fix.sh ]
then
    rm ${SCRIPTS}/temporary-fix.sh
    wget -q "${STATIC}/temporary-fix.sh" -P ${SCRIPTS}
else
    wget -q "${STATIC}/temporary-fix.sh" -P ${SCRIPTS}
fi

# Get timezone.sh
if [ -f ${SCRIPTS}/timezone.sh ]
then
    rm ${SCRIPTS}/timezone.sh
    wget -q "${GITLAB}/timezone.sh" -P ${SCRIPTS}
else
    wget -q "${GITLAB}/timezone.sh" -P ${SCRIPTS}
fi

# Get change-NCADMIN-password.sh
if [ -f ${SCRIPTS}/change-NCADMIN-password.sh ]
then
    rm ${SCRIPTS}/change-NCADMIN-password.sh
    wget -q "${GITLAB}/change-NCADMIN-password.sh" -P ${SCRIPTS}
else
    wget -q "${GITLAB}/change-NCADMIN-password.sh" -P ${SCRIPTS}
fi

# Lets Encrypt
if [ -f "$SCRIPTS"/activate-ssl.sh ]
then
    rm "$SCRIPTS"/activate-ssl.sh
    wget -q $LETS_ENC/activate-ssl.sh -P "$SCRIPTS"
else
    wget -q $LETS_ENC/activate-ssl.sh -P "$SCRIPTS"
fi
if [ ! -f "$SCRIPTS"/activate-ssl.sh ]
then
    echo "activate-ssl failed"
    echo "Script failed to download. Please run: 'sudo bash $SCRIPTS/nextcloud-startup-script.sh' again."
    exit 1
fi

mv $SCRIPTS/index.php $HTML/index.php && rm -f $HTML/html/index.html
chmod 750 $HTML/index.php && chown www-data:www-data $HTML/index.php

# Change 000-default to $WEB_ROOT
sed -i "s|DocumentRoot /var/www/html|DocumentRoot $HTML|g" /etc/apache2/sites-available/000-default.conf

# Make $SCRIPTS excutable
chmod +x -R $SCRIPTS
chown root:root -R $SCRIPTS

# Allow $UNIXUSER to run figlet script
chown "$UNIXUSER":"$UNIXUSER" "$SCRIPTS/nextcloud.sh"

clear
echo "+--------------------------------------------------------------------+"
echo "| This script will configure your Nextcloud and activate SSL.        |"
echo "| It will also do the following:                                     |"
echo "|                                                                    |"
echo "| - Set new keyboard layout                                          |"
echo "| - Change timezone                                                  |"
echo "| - Generate new SSH keys for the server                             |"
echo "| - Generate new MySQL password                                      |"
echo "| - Configure UTF8mb4 (4-byte support for MySQL)                     |"
echo "| - Install phpMyadmin and make it secure                            |"
echo "| - Install selected apps and automatically configure them           |"
echo "| - Detect and set hostname                                          |"
echo "| - Upgrade your system and Nextcloud to latest version              |"
echo "| - Set secure permissions to Nextcloud                              |"
#echo "| - Set new passwords to Linux and Nextcloud                         |"
#echo "| - Set static IP to the system (you have to set the same IP in      |"
#echo "|   your router) https://www.techandme.se/open-port-80-443/          |"
#echo "|   We don't set static IP if you run this on a *remote* VPS.        |"
echo "|                                                                    |"
echo "|   The script will take about 10 minutes to finish,                 |"
echo "|   depending on your internet connection.                           |"
echo "|                                                                    |"
echo "+--------------------------------------------------------------------+"
any_key "Press any key to start the script..."
clear


# Set keyboard layout
echo "Current keyboard layout is $(localectl status | grep "Layout" | awk '{print $3}')"
if [[ "no" == $(ask_yes_or_no "Do you want to change keyboard layout?") ]]
then
    echo "Not changing keyboard layout..."
    sleep 1
    clear
else
    dpkg-reconfigure keyboard-configuration
clear
fi

# Change Timezone
if [[ "yes" == $(ask_yes_or_no "Do you want to change your timezone?") ]]
then
	dpkg-reconfigure tzdata
	sleep 3
  any_key "Press any key to continue..."
else
    echo
    echo "OK, Current timezone is $(cat /etc/timezone)"
    echo "However, if you want to change it later, just type: sudo bash $SCRIPTS/timezone.sh"
    any_key "Press any key to continue..."
fi
clear

# Pretty URLs
echo "Setting RewriteBase to \"/\" in config.php..."
chown -R www-data:www-data $NCPATH
sudo -u www-data php $NCPATH/occ config:system:set htaccess.RewriteBase --value="/"
sudo -u www-data php $NCPATH/occ maintenance:update:htaccess
bash $SECURE & spinner_loading

# Generate new SSH Keys
printf "\nGenerating new SSH keys for the server...\n"
rm -v /etc/ssh/ssh_host_*
dpkg-reconfigure openssh-server

# Generate new MySQL password
echo "Generating new MySQL password..."
if bash "$SCRIPTS/change_mysql_pass.sh" && wait
then
   rm "$SCRIPTS/change_mysql_pass.sh"
   {
   echo "[mysqld]"
   echo "innodb_large_prefix=on"
   echo "innodb_file_format=barracuda"
   echo "innodb_file_per_table=1"
   } >> /root/.my.cnf
fi
echo "Please make a record of the new MySQL password above"
any_key "Press any key to continue..."

# Enable UTF8mb4 (4-byte support)
NCDB=nextcloud_db
PW_FILE=/var/mysql_password.txt
printf "\nEnabling UTF8mb4 support on $NCDB....\n"
echo "Please be patient, it may take a while."
sudo /etc/init.d/mysql restart & spinner_loading
RESULT="mysqlshow --user=root --password=$(cat $PW_FILE) $NCDB| grep -v Wildcard | grep -o $NCDB"
if [ "$RESULT" == "$NCDB" ]; then
    check_command mysql -u root -e "ALTER DATABASE $NCDB CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"
    wait
fi
check_command sudo -u www-data $NCPATH/occ config:system:set mysql.utf8mb4 --type boolean --value="true"
check_command sudo -u www-data $NCPATH/occ maintenance:repair

# Install phpMyadmin
run_app_script phpmyadmin_install_ubuntu16
clear

cat << LETSENC
+-----------------------------------------------+
|  The following script will install a trusted  |
|  SSL certificate through Lets Encrypt.        |
+-----------------------------------------------+
LETSENC

# Lets Encrypt
if [[ "yes" == $(ask_yes_or_no "Do you want to install SSL?") ]]
then
    bash $SCRIPTS/activate-ssl.sh
else
    echo
    echo "OK, but please note that some apps like Collabora will not work without ssl being enabled"
    echo "If you want to run this script later, just type: sudo bash $SCRIPTS/activate-ssl.sh"
    any_key "Press any key to continue..."
fi
clear

whiptail --title "Which apps do you want to install?" --checklist --separate-output "Automatically configure and install selected apps\nSelect by pressing the spacebar" "$WT_HEIGHT" "$WT_WIDTH" 4 \
"Collabora" "(Online editing)   " OFF \
"Nextant" "(Full text search)   " OFF \
"Passman" "(Password storage)   " OFF \
"Spreed.ME" "(Video calls)   " OFF 2>results

while read -r -u 9 choice
do
    case $choice in
        Collabora)
            run_app_script collabora
        ;;

        Nextant)
            run_app_script nextant
        ;;

        Passman)
            run_app_script passman
        ;;

        Spreed.ME)
            run_app_script spreedme
        ;;

        *)
        ;;
    esac
done 9< results
rm -f results
clear

# Add extra security
if [[ "yes" == $(ask_yes_or_no "Do you want to add extra security, based on this: http://goo.gl/gEJHi7 ?") ]]
then
    bash $SCRIPTS/security.sh
    rm "$SCRIPTS"/security.sh
else
    echo
    echo "OK, but if you want to run it later, just type: sudo bash $SCRIPTS/security.sh"
    any_key "Press any key to continue..."
fi
clear

## Change UNIXUSER password (Skipped)
#printf "${Color_Off}\n"
#echo "For better security, change the system user password for [$UNIXUSER]"
#any_key "Press any key to change password for system user..."
#while true
#do
#    sudo passwd "$UNIXUSER" && break
#done
#echo
#clear

## Change NCADMIN password
NCADMIN=$(sudo -u www-data php $NCPATH/occ user:list | awk '{print $3}')
printf "${Color_Off}\n"
echo "For better security, change the Default Nextcloud password for [$NCADMIN]"
echo "The current password for $NCADMIN is [$NCPASS]"
if [[ "yes" == $(ask_yes_or_no "Do you want to change the Default Nextcloud password for [$NCADMIN] ?") ]]
then
    sudo -u www-data php "$NCPATH/occ" user:resetpassword "$NCADMIN" && break
else
    echo
    echo "OK, but your Nextcloud server will not be propperly secured until this password is updated."
    echo "To update this password from the CLI, just type: sudo bash $SCRIPTS/change-NCADMIN-password.sh"
    any_key "Press any key to continue..."
fi
clear

# Fixes https://github.com/nextcloud/vm/issues/58
a2dismod status
service apache2 reload

# Increase max filesize (expects that changes are made in /etc/php/7.0/apache2/php.ini)
# Here is a guide: https://www.techandme.se/increase-max-file-size/
VALUE="# php_value upload_max_filesize 513M"
if ! grep -Fxq "$VALUE" $NCPATH/.htaccess
then
        sed -i 's/  php_value upload_max_filesize 513M/# php_value upload_max_filesize 513M/g' $NCPATH/.htaccess
        sed -i 's/  php_value post_max_size 513M/# php_value post_max_size 513M/g' $NCPATH/.htaccess
        sed -i 's/  php_value memory_limit 512M/# php_value memory_limit 512M/g' $NCPATH/.htaccess
fi

# Add temporary fix if needed
bash $SCRIPTS/temporary-fix.sh
rm "$SCRIPTS"/temporary-fix.sh

# Prepare every bootup
#check_command run_static_script change-ncadmin-profile
check_command run_gitlab_script change-root-profile_every-run

# Cleanup 1
sudo -u www-data php "$NCPATH/occ" maintenance:repair
rm -f "$SCRIPTS/ip.sh"
rm -f "$SCRIPTS/test_connection.sh"
#rm -f "$SCRIPTS/instruction.sh"
rm -f "$NCDATA/nextcloud.log"
#rm -f "$SCRIPTS/nextcloud-startup-script.sh"
find /root "/home/$UNIXUSER" -type f \( -name '*.sh*' -o -name '*.html*' -o -name '*.tar*' -o -name '*.zip*' \) -delete
sed -i "s|instruction.sh|nextcloud.sh|g" "/home/$UNIXUSER/.bash_profile"

truncate -s 0 \
    /root/.bash_history \
    "/home/$UNIXUSER/.bash_history" \
    /var/spool/mail/root \
    "/var/spool/mail/$UNIXUSER" \
    /var/log/apache2/access.log \
    /var/log/apache2/error.log \
    /var/log/cronjobs_success.log

sed -i "s|sudo -i||g" "/home/$UNIXUSER/.bash_profile"
cat << RCLOCAL > "/etc/rc.local"
#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

exit 0

RCLOCAL
clear

# Upgrade system
echo "System will now upgrade..."
bash $SCRIPTS/update.sh

# Cleanup 2
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb
CLEARBOOT=$(dpkg -l linux-* | awk '/^ii/{ print $2}' | grep -v -e "$(uname -r | cut -f1,2 -d"-")" | grep -e "[0-9]" | xargs sudo apt -y purge)
echo "$CLEARBOOT"
# Delete MySQL password from /root/.my.cnf (for security resons)
sed -i '/password/d' /root/.my.cnf

ADDRESS2=$(grep "address" /etc/network/interfaces | awk '$1 == "address" { print $2 }')
# Success!
clear
printf "%s\n""${Green}"
echo    "+--------------------------------------------------------------------+"
echo    "|      Congratulations! You have successfully installed Nextcloud!   |"
echo    "|                                                                    |"
printf "|         ${Color_Off}Login to Nextcloud in your browser: ${Cyan}\"$ADDRESS2\"${Green}         |\n"
echo    "|                                                                    |"
printf "|         ${Color_Off}Publish your server online! ${Cyan}https://goo.gl/iUGE2U${Green}          |\n"
echo    "|                                                                    |"
printf "|         ${Color_Off}To login to MySQL just type: ${Cyan}'mysql -u root -p'${Green}               |\n"
echo    "|                                                                    |"
printf "|   ${Color_Off}To update this server just type: ${Cyan}'sudo bash /var/scripts/update.sh'${Green}  |\n"
echo    "|                                                                    |"
echo    "+--------------------------------------------------------------------+"
printf "${Color_Off}\n"

# Set trusted domain in config.php
bash "$SCRIPTS"/trusted.sh
rm -f "$SCRIPTS"/trusted.sh

# Prefer IPv6
sed -i "s|precedence ::ffff:0:0/96  100|#precedence ::ffff:0:0/96  100|g" /etc/gai.conf

# Reboot
#rm -f "$SCRIPTS/nextcloud-startup-script.sh"
any_key "Installation finished, press any key to reboot system..."
reboot
