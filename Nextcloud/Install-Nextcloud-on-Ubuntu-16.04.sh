#!/bin/bash

# Tech and Me © - 2017, https://www.techandme.se/
# Tweaks to Original scripts by Clamshell Data, https://clamshelldata.com

# Temp fix for weird issues which is encountered intermittently
#cd /var/lib/apt
#mv lists lists.old
#mkdir -p lists/partial
#cd

# Delete all old old kernel versions
#dpkg --get-selections | grep linux-image #(Shows all kernel versions)
sudo apt-get -y remove --purge linux-image-4.4.0-31-generic
sudo apt-get -y remove --purge linux-image-4.4.0-66-generic
sudo apt-get -y remove --purge linux-image-4.4.0-72-generic

# Prefer IPv4
sed -i "s|#precedence ::ffff:0:0/96  100|precedence ::ffff:0:0/96  100|g" /etc/gai.conf

echo "Setting Variables, Functions, and Colors"
# shellcheck disable=2034,2059
true
# shellcheck source=lib.sh
FIRST_IFACE=1 && CHECK_CURRENT_REPO=1 . <(curl -sL https://raw.githubusercontent.com/nextcloud/vm/master/lib.sh)
unset FIRST_IFACE
unset CHECK_CURRENT_REPO

# Custom Variables
GITLAB="https://gitlab.com/hungrykanamit/Nextcloud/raw/master/Nextcloud/Scripts/"

# Custom Functions
  # run_gitlab_script name_of_script
  run_gitlab_script() {
      # Get ${1} gitlab
      rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
      if wget -q "${GITLAB}/${1}.sh" -P "$SCRIPTS"
      then
          bash "${SCRIPTS}/${1}.sh"
          rm -f "${SCRIPTS}/${1}.sh"
      elif wget -q "${GITLAB}/${1}.php" -P "$SCRIPTS"
      then
          php "${SCRIPTS}/${1}.php"
          rm -f "${SCRIPTS}/${1}.php"
      elif wget -q "${GITLAB}/${1}.py" -P "$SCRIPTS"
      then
          python "${SCRIPTS}/${1}.py"
          rm -f "${SCRIPTS}/${1}.py"
      else
          echo "Downloading ${1} failed"
          echo "Script failed to download. Please run: 'sudo wget ${GITLAB}/${1}.sh|php|py' again."
          sleep 3
      fi
  }

# Create $SCRIPTS dir
if [ ! -d "$SCRIPTS" ]
then
    mkdir -p "$SCRIPTS"
fi

# Change DNS
echo "Setting DNS"
if ! [ -x "$(command -v resolvconf)" ]
then
    apt install resolvconf -y -q
    dpkg-reconfigure resolvconf
fi

echo "nameserver 8.8.8.8" > /etc/resolvconf/resolv.conf.d/base
echo "nameserver 8.8.4.4" >> /etc/resolvconf/resolv.conf.d/base

# Check network
echo "Checking Network"
if ! [ -x "$(command -v nslookup)" ]
then
    apt install dnsutils -y -q
fi
if ! [ -x "$(command -v ifup)" ]
then
    apt install ifupdown -y -q
fi
sudo ifdown "$IFACE" && sudo ifup "$IFACE"
if ! nslookup google.com
then
    echo "Network NOT OK. You must have a working Network connection to run this script."
    exit 1
fi

# Set locales
echo "Setting locales"
apt install language-pack-en-base -y
sudo locale-gen "sv_SE.UTF-8" && sudo dpkg-reconfigure --frontend=noninteractive locales

apt update -q4 & spinner_loading
apt install python-pip -y
pip install \
--upgrade pip \
#apt-select
#apt-select -m up-to-date -t 5 -c
#sudo cp /etc/apt/sources.list /etc/apt/sources.list.backup && \

# Update system
apt update -q4 & spinner_loading

# Write MySQL pass to file and keep it safe
echo "$MYSQL_PASS" > $PW_FILE
chmod 600 $PW_FILE
chown root:root $PW_FILE

# Install MYSQL 5.7
echo "Installing MySQL"
apt install software-properties-common -y
echo "mysql-server-5.7 mysql-server/root_password password $MYSQL_PASS" | debconf-set-selections
echo "mysql-server-5.7 mysql-server/root_password_again password $MYSQL_PASS" | debconf-set-selections
check_command apt install mysql-server-5.7 -y

# mysql_secure_installation
apt -y install expect
SECURE_MYSQL=$(expect -c "
set timeout 10
spawn mysql_secure_installation
expect \"Enter current password for root:\"
send \"$MYSQL_PASS\r\"
expect \"Would you like to setup VALIDATE PASSWORD plugin?\"
send \"n\r\"
expect \"Change the password for root ?\"
send \"n\r\"
expect \"Remove anonymous users?\"
send \"y\r\"
expect \"Disallow root login remotely?\"
send \"y\r\"
expect \"Remove test database and access to it?\"
send \"y\r\"
expect \"Reload privilege tables now?\"
send \"y\r\"
expect eof
")
echo "$SECURE_MYSQL"
apt -y purge expect

#SLIMClEAN
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb

# Install Apache
echo "Installing Apache"
check_command apt install apache2 -y
a2enmod rewrite \
        headers \
        env \
        dir \
        mime \
        ssl \
        setenvif

#SLIMClEAN
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb

# Install PHP 7.0
echo "Installing PHP"
apt update -q4 & spinner_loading
check_command apt install -y \
    libapache2-mod-php7.0 \
    php7.0-common \
    php7.0-mysql \
    php7.0-intl \
    php7.0-mcrypt \
    php7.0-ldap \
    php7.0-imap \
    php7.0-cli \
    php7.0-gd \
    php7.0-pgsql \
    php7.0-json \
    php7.0-sqlite3 \
    php7.0-curl \
    php7.0-xml \
    php7.0-zip \
    php7.0-mbstring \
    php-smbclient

# Enable SMB client
# echo '# This enables php-smbclient' >> /etc/php/7.0/apache2/php.ini
# echo 'extension="smbclient.so"' >> /etc/php/7.0/apache2/php.ini

#SLIMClEAN
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb

# Download and validate Nextcloud package
echo "Downloading Nextcloud"
check_command download_verify_nextcloud_stable

if [ ! -f "$HTML/$STABLEVERSION.tar.bz2" ]
then
    echo "Aborting,something went wrong with the download of $STABLEVERSION.tar.bz2"
    exit 1
fi

# Extract package
tar -xjf "$HTML/$STABLEVERSION.tar.bz2" -C "$HTML" & spinner_loading
rm "$HTML/$STABLEVERSION.tar.bz2"

# Secure permissions
echo "Securing permissions"
download_static_script setup_secure_permissions_nextcloud
bash $SECURE & spinner_loading

# Install Nextcloud
echo "Installing Nextcloud"
cd "$NCPATH"
check_command sudo -u www-data php occ maintenance:install \
    --data-dir "$NCDATA" \
    --database "mysql" \
    --database-name "nextcloud_db" \
    --database-user "root" \
    --database-pass "$MYSQL_PASS" \
    --admin-user "$NCUSER" \
    --admin-pass "$NCPASS"
echo
echo "Nextcloud version:"
sudo -u www-data php "$NCPATH"/occ status
sleep 3
echo

# Prepare cron.php to be run every 15 minutes
echo "Setting Contabs"
crontab -u www-data -l | { cat; echo "*/15  *  *  *  * php -f $NCPATH/cron.php > /dev/null 2>&1"; } | crontab -u www-data -

# Change values in php.ini (increase max file size)
echo "Setting PHP Values"
# max_execution_time
sed -i "s|max_execution_time = 30|max_execution_time = 3500|g" /etc/php/7.0/apache2/php.ini
# max_input_time
sed -i "s|max_input_time = 60|max_input_time = 3600|g" /etc/php/7.0/apache2/php.ini
# memory_limit
sed -i "s|memory_limit = 128M|memory_limit = 512M|g" /etc/php/7.0/apache2/php.ini
# post_max
sed -i "s|post_max_size = 8M|post_max_size = 1100M|g" /etc/php/7.0/apache2/php.ini
# upload_max
sed -i "s|upload_max_filesize = 2M|upload_max_filesize = 1000M|g" /etc/php/7.0/apache2/php.ini

# Increase max filesize (expects that changes are made in /etc/php/7.0/apache2/php.ini)
# Here is a guide: https://www.techandme.se/increase-max-file-size/
VALUE="# php_value upload_max_filesize 511M"
if ! grep -Fxq "$VALUE" "$NCPATH"/.htaccess
then
        sed -i 's/  php_value upload_max_filesize 511M/# php_value upload_max_filesize 511M/g' "$NCPATH"/.htaccess
        sed -i 's/  php_value post_max_size 511M/# php_value post_max_size 511M/g' "$NCPATH"/.htaccess
        sed -i 's/  php_value memory_limit 512M/# php_value memory_limit 512M/g' "$NCPATH"/.htaccess
fi

# Install Figlet
apt install figlet -y

# Generate $HTTP_CONF
echo "Generate VirtualHosts"
if [ ! -f $HTTP_CONF ]
then
    touch "$HTTP_CONF"
    cat << HTTP_CREATE > "$HTTP_CONF"
<VirtualHost *:80>

### YOUR SERVER ADDRESS ###
#    ServerAdmin admin@example.com
#    ServerName example.com
#    ServerAlias subdomain.example.com

### SETTINGS ###
    DocumentRoot $NCPATH

    <Directory $NCPATH>
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
    Satisfy Any
    </Directory>

    <IfModule mod_dav.c>
    Dav off
    </IfModule>

    <Directory "$NCDATA">
    # just in case if .htaccess gets disabled
    Require all denied
    </Directory>

    SetEnv HOME $NCPATH
    SetEnv HTTP_HOME $NCPATH

</VirtualHost>
HTTP_CREATE
    echo "$HTTP_CONF was successfully created"
fi

# Generate $SSL_CONF
if [ ! -f $SSL_CONF ]
then
    touch "$SSL_CONF"
    cat << SSL_CREATE > "$SSL_CONF"
<VirtualHost *:443>
    Header add Strict-Transport-Security: "max-age=15768000;includeSubdomains"
    SSLEngine on

### YOUR SERVER ADDRESS ###
#    ServerAdmin admin@example.com
#    ServerName example.com
#    ServerAlias subdomain.example.com

### SETTINGS ###
    DocumentRoot $NCPATH

    <Directory $NCPATH>
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
    Satisfy Any
    </Directory>

    <IfModule mod_dav.c>
    Dav off
    </IfModule>

    <Directory "$NCDATA">
    # just in case if .htaccess gets disabled
    Require all denied
    </Directory>

    SetEnv HOME $NCPATH
    SetEnv HTTP_HOME $NCPATH

### LOCATION OF CERT FILES ###
    SSLCertificateFile /etc/ssl/certs/ssl-cert-snakeoil.pem
    SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key
</VirtualHost>
SSL_CREATE
    echo "$SSL_CONF was successfully created"
fi

# Enable new config
a2ensite nextcloud_ssl_domain_self_signed.conf
a2ensite nextcloud_http_domain_self_signed.conf
a2dissite default-ssl
service apache2 restart

## Set config values
# Experimental apps
sudo -u www-data php "$NCPATH"/occ config:system:set appstore.experimental.enabled --value="true"
# Default mail server as an example (make this user configurable?)
sudo -u www-data php "$NCPATH"/occ config:system:set mail_smtpmode --value="smtp"
sudo -u www-data php "$NCPATH"/occ config:system:set mail_smtpauth --value="1"
sudo -u www-data php "$NCPATH"/occ config:system:set mail_smtpport --value="465"
sudo -u www-data php "$NCPATH"/occ config:system:set mail_smtphost --value="smtp.gmail.com"
sudo -u www-data php "$NCPATH"/occ config:system:set mail_smtpauthtype --value="LOGIN"
sudo -u www-data php "$NCPATH"/occ config:system:set mail_from_address --value="www.example.com"
sudo -u www-data php "$NCPATH"/occ config:system:set mail_domain --value="gmail.com"
sudo -u www-data php "$NCPATH"/occ config:system:set mail_smtpsecure --value="ssl"
sudo -u www-data php "$NCPATH"/occ config:system:set mail_smtpname --value="www.example.com@gmail.com"
sudo -u www-data php "$NCPATH"/occ config:system:set mail_smtppassword --value="vinr vhpa jvbh hovy"

whiptail --title "Which apps/programs do you want to install?" --checklist --separate-output "" 10 40 3 \
"Calendar" "              " on \
"Contacts" "              " on \
"Webmin" "              " on 2>results

while read -r -u 9 choice
do
    case "$choice" in
        Calendar)
            run_app_script calendar
        ;;
        Contacts)
            run_app_script contacts
        ;;
        Webmin)
            run_app_script webmin
        ;;
        *)
        ;;
    esac
done 9< results
rm -f results

#SLIMClEAN
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb

# Upgrade
apt full-upgrade & spinner_loading

#SLIMClEAN
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb

# Prep for libreoffice
echo "Getting ready to install libreoffice"
#sudo apt-get install localepurge
apt install --no-install-recommends -y fontconfig fonts-opensymbol libabw-0.1-1v5 libboost-date-time1.58.0 libcairo2 libclucene-contribs1v5 libclucene-core1v5 libcmis-0.5-5v5 libcolamd2.9.1 libdrm-amdgpu1 libdrm-intel1 libdrm-nouveau2 libdrm-radeon1 libe-book-0.1-1 libeot0 libetonyek-0.1-1 libexttextcat-2.0-0 libexttextcat-data libgl1-mesa-dri libgl1-mesa-glx libglapi-mesa libglew1.13 libgraphite2-3 libharfbuzz-icu0 libharfbuzz0b libhunspell-1.3-0 libhyphen0 libice6 liblangtag-common liblangtag1 liblcms2-2 libllvm3.8 libltdl7 libmhash2 libmwaw-0.3-3 libmythes-1.2-0 libneon27-gnutls libnspr4 libnss3 libnss3-nssdb libodfgen-0.1-1 libpciaccess0 libpixman-1-0 libraptor2-0 librasqal3 librdf0 libreoffice-base-core libreoffice-common
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb
apt install --no-install-recommends -y libreoffice-core libreoffice-style-elementary libreoffice-style-galaxy librevenge-0.0-0 libsm6 libsuitesparseconfig4.4.6 libwpd-0.10-10 libwpg-0.3-3 libwps-0.4-4 libx11-xcb1 libxcb-dri2-0 libxcb-dri3-0 libxcb-glx0 libxcb-present0 libxcb-render0 libxcb-shm0 libxcb-sync1 libxdamage1 libxfixes3 libxinerama1 libxrandr2 libxrender1 libxshmfence1 libxxf86vm1 libyajl2 lp-solve uno-libs3 ure x11-common

#SLIMClEAN
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb

# Install Libreoffice Writer to be able to read MS documents.
echo "Installing Libreoffice"
sudo apt install --no-install-recommends libreoffice-writer -y
sudo -u www-data php "$NCPATH"/occ config:system:set preview_libreoffice_path --value="/usr/bin/libreoffice"

#SLIMClEAN
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb

# Get needed scripts for first bootup
echo "Downloading Scripts from the Web"

# Getting nextcloud-first-run-startup.sh
if [ ! -f "$SCRIPTS"/nextcloud-first-run-startup.sh ]
then
check_command wget -q "$GITLAB"/nextcloud-first-run-startup.sh -P "$SCRIPTS"
fi

# Getting instruction.sh
if [ ! -f "$SCRIPTS"/instruction.sh ]
then
check_command wget -q "$GITLAB"/instruction.sh -P "$SCRIPTS"
fi

# Getting history.sh
download_static_script history

# Make $SCRIPTS excutable
chmod +x -R "$SCRIPTS"
chown root:root -R "$SCRIPTS"

# Prepare first bootup
#check_command run_static_script change-ncadmin-profile
check_command run_gitlab_script change-root-profile_first-run

# Install Redis
echo "Installing Redis"
run_static_script redis-server-ubuntu16

#SLIMClEAN
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb

# Upgrade
apt update -q4 & spinner_loading
apt dist-upgrade -y

# Remove LXD (always shows up as failed during boot)
apt purge lxd -y

# Cleanup
echo "Cleaning Up"
CLEARBOOT=$(dpkg -l linux-* | awk '/^ii/{ print $2}' | grep -v -e ''"$(uname -r | cut -f1,2 -d"-")"'' | grep -e '[0-9]' | xargs sudo apt -y purge)
echo "$CLEARBOOT"
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb
apt-get autoremove --purge
find /root "/home/$UNIXUSER" -type f \( -name '*.sh*' -o -name '*.html*' -o -name '*.tar*' -o -name '*.zip*' \) -delete

# Set secure permissions final (./data/.htaccess has wrong permissions otherwise)
bash $SECURE & spinner_loading

# Shutdown, next boot is for Clent setup
echo "Installation done, system will now shutdown so a template can be made..."
shutdown -h now
